<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Var_dump</title>
</head>
<body>
<?php
$nama = 'Agus';
var_dump ($nama); // Hasil: string(4) "Agus“

$siswa = array(
	'nama' => array ('Arif', 'Beta', 'Cici'),
	'jurusan' 	=> 'Informatika',
	'semester'=> 1,
	1 => 'Jakarta',
	2 => 'Surabaya'
	);
		 
echo '<pre>';  var_dump($siswa); echo '</pre>';
?>



</body>
</html>